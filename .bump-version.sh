bump_build
bump_sync package.json 's|"version":.*|"version": "'$(cat .release-version)'",|'
bump_sync Cargo.toml 's|^version =.*|version = "'$(cat .release-version)'"|'
