# CHANGELOG

## Version : 1.5.0

- fix: js : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!25


## Version : 1.4.0

- fix: Api code : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!23


## Version : 1.3.0

- add: typescript : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!21
- add: envoy : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!20
- add: grpc : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!19


## Version : 1.2.0

- remove: post install script : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!17


## Version : 1.1.0

- fix: package.json : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!15
- update: bump_version : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!14



## Version : 1.0.0

- fix: gitlab ci : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!12
- production ready! : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!10
- fix: bump version : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!9
- fix: package : See merge request getto-systems-base/labo/misc/parcel/without-auth/web!8



## Version : 0.1.0


