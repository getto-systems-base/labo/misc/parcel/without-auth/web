use tonic::{transport::Server, Request, Response, Status};

use greet::greeter_server::{Greeter, GreeterServer};
use greet::{HelloReply, HelloRequest};

use labo_parcel_without_auth_core;

pub mod greet {
    tonic::include_proto!("greet");
}

#[derive(Debug, Default)]
pub struct MyGreeter {}

#[tonic::async_trait]
impl Greeter for MyGreeter {
    async fn say_hello(
        &self,
        request: Request<HelloRequest>,
    ) -> Result<Response<HelloReply>, Status> {
        println!("Got a request: {:?}", request);

        let reply = greet::HelloReply {
            message: labo_parcel_without_auth_core::greet(&request.into_inner().name).into(),
        };

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "0.0.0.0:5000".parse()?;
    let greeter = MyGreeter::default();

    Server::builder()
        .add_service(GreeterServer::new(greeter))
        .serve(addr)
        .await?;

    Ok(())
}
