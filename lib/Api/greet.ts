import {HelloRequest, HelloReply} from './pb/greet_pb';
import {GreeterClient} from './pb/GreetServiceClientPb';

const client = new GreeterClient('http://192.168.0.110:8081');

export function init(app) {
  app.ports.greetRequest.subscribe((data: {message: string}) => {
    const request = new HelloRequest();
    request.setName(data.message);

    client.sayHello(request, {}, (err, response: HelloReply) => {
      app.ports.greetResponse.send({
        error: err,
        response: {
          message: response.getMessage(),
        },
      });
    });
  });
}
