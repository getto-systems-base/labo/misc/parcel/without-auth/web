"use strict";
/**
 * @fileoverview gRPC-Web generated client stub for greet
 * @enhanceable
 * @public
 */
exports.__esModule = true;
// GENERATED CODE -- DO NOT EDIT!
var grpcWeb = require("grpc-web");
var greet_pb_1 = require("./greet_pb");
var GreeterClient = /** @class */ (function () {
    function GreeterClient(hostname, credentials, options) {
        this.methodInfoSayHello = new grpcWeb.AbstractClientBase.MethodInfo(greet_pb_1.HelloReply, function (request) {
            return request.serializeBinary();
        }, greet_pb_1.HelloReply.deserializeBinary);
        if (!options)
            options = {};
        if (!credentials)
            credentials = {};
        options['format'] = 'text';
        this.client_ = new grpcWeb.GrpcWebClientBase(options);
        this.hostname_ = hostname;
        this.credentials_ = credentials;
        this.options_ = options;
    }
    GreeterClient.prototype.sayHello = function (request, metadata, callback) {
        return this.client_.rpcCall(this.hostname_ +
            '/greet.Greeter/SayHello', request, metadata || {}, this.methodInfoSayHello, callback);
    };
    return GreeterClient;
}());
exports.GreeterClient = GreeterClient;
