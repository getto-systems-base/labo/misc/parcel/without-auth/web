port module Api.Greet exposing (Request, Reply, request, reply)


type alias Response a =
    { response : Maybe a
    , error : Maybe String
    }

map : (Result String a -> msg) -> (Response a -> msg)
map msg response =
    case response.response of
        Just res -> Ok res |> msg
        _ -> Err (response.error |> Maybe.withDefault "") |> msg


port greetRequest : Request -> Cmd msg
port greetResponse : (Response Reply -> msg) -> Sub msg

type alias Request =
    { message : String
    }

type alias Reply =
    { message : String
    }

request : Request -> Cmd msg
request = greetRequest

reply : (Result String Reply -> msg) -> Sub msg
reply msg = greetResponse (msg |> map)
