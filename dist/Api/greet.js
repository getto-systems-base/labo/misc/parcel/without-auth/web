"use strict";
exports.__esModule = true;
var greet_pb_1 = require("./pb/greet_pb");
var GreetServiceClientPb_1 = require("./pb/GreetServiceClientPb");
var client = new GreetServiceClientPb_1.GreeterClient('http://192.168.0.110:30081');
function init(app) {
    app.ports.greetRequest.subscribe(function (data) {
        var request = new greet_pb_1.HelloRequest();
        request.setName(data.message);
        client.sayHello(request, {}, function (err, response) {
            app.ports.greetResponse.send({
                error: err,
                response: {
                    message: response.getMessage()
                }
            });
        });
    });
}
exports.init = init;
